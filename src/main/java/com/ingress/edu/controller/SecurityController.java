package com.ingress.edu.controller;

import com.ingress.edu.dto.HelloData;
import java.security.Principal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/security")
@Slf4j
public class SecurityController {

  @GetMapping("/hello")
  public HelloData helloData() {
    return new HelloData("hello message");
  }

  @GetMapping("/authenticated")
  public HelloData authenticated(Principal principal) {
    log.info(" " + principal);
    return new HelloData(
        "hello message, Welcome to system and Username equal to " + principal.getName());
  }

  @GetMapping("/authenticated/{id}")
  public HelloData authenticatedById(@PathVariable long id) {
    return new HelloData("hello message, Welcome to system with id equal to " + id);
  }

  @PostMapping("/post/")
  public HelloData save(@RequestBody HelloData helloData, Principal principal) {
    return new HelloData(
        "hello message, Welcome to system with id equal to " + helloData.getMessage() +
            " user equal to " + principal.getName());
  }

  @GetMapping("/get")
  public HelloData get(Principal principal) {
    return new HelloData(
        "hello message, Welcome to system with id equal to " +
            " user equal to " + principal.getName());
  }

}
