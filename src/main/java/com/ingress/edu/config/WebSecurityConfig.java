package com.ingress.edu.config;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

import com.ingress.edu.filter.FilterConfigureAdaptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig {
  private final FilterConfigureAdaptor configureAdaptor;

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.apply(configureAdaptor);
    http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        .and()

        .authorizeRequests().antMatchers("/security/hello").permitAll()

        .and()

        .authorizeRequests().antMatchers(GET, "security/authenticated/*").hasAnyRole("ADMIN")

        .and()

        .authorizeRequests().antMatchers(POST, "/security/post/").hasAnyRole("ADMIN")

        .and()


        .httpBasic();
    return http.build();
  }


}
