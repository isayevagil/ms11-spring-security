package com.ingress.edu;

import com.ingress.edu.model.Authority;
import com.ingress.edu.model.User;
import com.ingress.edu.repository.UserRepository;
import com.ingress.edu.service.JwtService;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Ms11SpringSecurityApplication implements CommandLineRunner {

  private final UserRepository userRepository;
  private final JwtService jwtService;

  public static void main(String[] args) {
    SpringApplication.run(Ms11SpringSecurityApplication.class, args);
  }


  @Override
  public void run(String... args) throws Exception {

    User user = new User();
    user.setUsername("bahram");
    user.setPassword(passwordEncoder().encode("12345"));
    user.setAccountNonExpired(true);
    user.setAccountNonLocked(true);
    user.setEnabled(true);
    user.setCredentialsNonExpired(true);

    Authority authority = new Authority();
    authority.setAuthority("ROLE_ADMIN");
    authority.setUser(user);
    user.setAuthorities(Set.of(authority));
    user.setAccessToken(jwtService.issueToken(user));
      userRepository.save(user);


    log.info(jwtService.issueToken(user));

//    log.info(jwtService.verifyToken());


  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }
}
